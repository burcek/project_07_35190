#include <iostream>
#include "gamestate.h"
using namespace std;



int main()
{
    system("chcp 1250");

    int _choice = -1;
    GameState state;

    while(_choice != 1) {

        state.clearScreen();
        //state.borderText("MILIONERZY");
        state.presentLogo();

        cout << "[1] - Rozpocznij gre" << endl;
        cout << "[2] - Tworcy" << endl;
        cout << "[3] - Zakoncz gre" << endl;
        cout << "(podaj numer opcji do wyboru)" << endl << endl;

        /*
            Kod zabezpieczajacy input na samym poczatku rozgrywki.
            Jesli poda sie znak ktorego nie mozna przekonwertowac do
            inta, petla sie powtorzy i ponownie poprosi cie o input.
        */
        while(1) {
            try {
                cout << "Twoja odpowiedz: ";
                string inputToCheck;
                cin >> inputToCheck;

                _choice = stoi(inputToCheck);

                if(_choice < 1 || _choice > 3) continue;
                break;
            }
            catch(exception ex) {
                continue;
            }
        }

        cin.ignore(1);

        switch(_choice) {

            case 1: {

                state.clearScreen();
                state.newQuestion();

                while(!state.gameLost) {

                    state.presentQuestion();
                    state.makeChoice();
                }

                break;

            } // start gry

            case 2: {
                state.clearScreen();
                state.showCreators();
                break;
            }

            case 3: {
                state.clearScreen();
                cout << "Konczenie pracy programu...";
                exit(0);
                break;
            } // koniec gry


            default:
                return 0;
            break;
        }
    }



    return 0;
}

