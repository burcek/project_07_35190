TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        gamestate.cpp \
        main.cpp

DISTFILES += \
    README.md \
    other/logo.txt \
    questions/question1.csv \
    questions/question2.csv \
    questions/question3.csv \
    questions/question4.csv \
    questions/question5.csv \
    questions/question6.csv \
    questions/question7.csv \
    questions/question8.csv \
    questions/question9.csv \
    questions/question10.csv \
    questions/question11.csv \
    questions/question12.csv \
    questions/question13.csv \
    questions/question14.csv \
    questions/question15.csv

HEADERS += \
    gamestate.h
