#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <ctime>
#include <algorithm>
#include <thread>
#include <chrono>

using namespace std;


class GameState {



    public:
        void newQuestion();
        void presentQuestion();
        void fiftyFifty();
        void makeChoice();
        void telephone();
        void poll();

        void clearScreen();
        void showCreators();
        void borderText(string);
        void sleep(int);
        void sleep_miliseconds(int);
        void presentLogo();
        void ending();
        void resign();
        void win();

        bool gameLost = 0;

    private:

        int activeRound = 1;
        const int isWindows = 1;

        bool useFiftyFifty = true;
        bool useTelephone = true;
        bool usePoll = true;

        vector<string> question;
        string correctAnswer;

        int moneyLevel[15] = {
            100, 200, 300, 500, 1000,
            2000, 4000, 8000, 16000, 32000,
            64000, 125000, 250000, 500000, 1000000
        };
        int playerScore = 0;
        int moneyCheckpoint = 0;


};

#endif // GAMESTATE_H
