Projekt 07 - 35190, 35187
# Milionerzy

**Milionerzy** to gra bazowana na brytyjskim show **'Who Wants to Be a Millionaire?'**.
Gra polega na odpowiedzeniu na 15 pytań w celu uzyskania jak największego wyniku, który jest także kwotą pieniężną, którą możesz zabrać do domu. Za poprawne odpowiedzenie na ostatnie pytanie, gracz wygrywa milion. Pomyłka skutkuje utratą nabitych funduszy, z wyjątkiem gwarantowanych pieniędzy. Gracz może zrezygnować po każdym pytaniu i zachować zdobyte pieniądze lub ryzykować i walczyć o kolejną kwotę.

### Projekt tworzą:

- **Bartosz Burzec** - przedstawiciel zespołu
- **Kacper Białas** - viceprzedstawiciel zespołu

### Budowa projektu

Projekt powstał w ramach przedmiotu *Narzędzia i Środowiska Programistyczne* jako projekt zaliczeniowy na koniec II semestru.

Całość kodu została napisana w języku **C++** przy pomocy środowiska **QT Creator**.

Budowa projektu, refaktoryzacja kodu i projektowanie rozwiązań zajęło 3 miesiące.

# Jak rozpocząć grę w Milionerów?

1. Sklonuj repozytorium i zapisz je w dowolnym miejscu:
```
git clone https://burcek@bitbucket.org/burcek/project_07_35190.git
```
2. Uruchom dowolny program do kompilowania kodu C++ (np. **QT Creator**, **CodeBlocks**),
3. Uruchom kod i rozpocznij rozgrywkę.

# Jak grać w Milionerów?

Po skompilowaniu kodu w oknie terminalu ujrzysz następujące menu:

![Menu główne](/assets/menu.png "Menu główne")

Wpisanie odpowiadającego przy opcji numeru wywoła opisane działanie. 
Wpsiując **1** rozpoczyna się pierwszy etap gry:

![Przykładowy etap](/assets/etap1.png "Przykładowy etap")

Aby wybrać którąś z podanych odpowiedzi, należy wpisać odpowiednio przypisaną odpowiedzi literę.
Jeżeli odpowiedź okaże się poprawną, wynik gracza wzrośnie i zostanie postawione pytanie czy chce kontynuować rozgrywkę.

![Decyzja o przebiegu gry](/assets/wybor.png "Decyzja o przebiegu gry")

Aby zadecydować należy wpisać jedną z dwóch podanych opcji. Wybranie opcji "gram" spowoduje rozpoczęcie kolejnego etapu gry.

# Koła ratunkowe

W wypadku pojawienia się ciężkiego pytania, gracz ma możliwość użycia trzech podpowiedzi. Aby którejkolwiek użyć, w miejscu wpisania odpowiedzi na pytanie należy wpisać jedną z podanych fraz aktywujących dane koło ratunkowe.

![Przykładowe koło ratunkowe](/assets/5050.png "Przykładowe koło ratunkowe")

Wybrane koła działają następująco:

- **50/50** - odrzucone zostają dwie błędne odpowiedzi.

- **pytanie** - pytanie do publiczności. Publiczność głosuje, co ich zdaniem jest poprawną odpowiedzią, a wynik zostaje wypisany na ekranie.

- **telefon** - telefon do przyjaciela. Przyjaciel podpowiada, która odpowiedź (jego zdaniem) jest poprawna.
**Po 5 pytaniu poziom wiedzy przyjaciela drastycznie spada.**

**Na każdą rozgrywkę można wykorzystać tylko 3 koła ratunkowe, po jednym z wyżej wymienonych.**

Gra kończy się w momencie podania złej odpowiedzi, rezygnacji z gry lub odpowiedzeniu na ostatnie pytanie za milion.

# Wykorzystane narzędzia

1. **QT Creator** - tworzenie kodu, struktury projektu, debugowanie i przeprowadzanie rozgrywki

2. **Microsoft Excel** - tworzenie plików z pytaniami

3. **Git** - organizacja projektu

4. **Discord** - konsultacje projektowe

# Struktura plików CSV

Aby poprawnie odczytywać pytania, każdy z utworzonych plików został utworzony według określonej struktury:

- pierwsze pole to pole zawierające treść pytania, np: "Ala ma..."

- drugie pole to poprawna odpowiedź: "...kota"

- trzecie, czwarte i piąte pole to złe odpowiedzi: "...psa", "...papugę", "...chomika"

Algorytm losujący wewnątrz programu pobiera losową linijkę z danego pliku, rozdziela ją a następnie umieszcza poszczególne pytania do tablicy z pytaniem i odpowiedziami.

**Poprawna odpowiedź zawsze znajduje się na drugim miejscu tablicy.**

# Podział prac projektowych
| Bartosz Burzec | Kacper Białas |
| -------------- | ------------- |
| utworzenie i nadzór repozytorium oraz projektu | tworzenie plików z pytaniami |
| pobieranie pytań z plików | utworzenie koła ratunkowego "telefon" |
| aktualizowanie dokumentacji | utworzenie koła ratunkowego "pytanie" |
| refaktoryzacja kodu | praca nad logiką gry|
| tworzenie klasy obejmującej logikę gry | tworzenie funkcji podsumowójących  |
| utworzenie koła ratunkowego "50/50" | tworzenie funkcji kończących grę |
| zarząd wizualizacją gry | tworzenie funkcji określających przebieg gry |
| praca nad menu głównym i prezentacją pytań | debugowanie |
| debugowanie i łatanie niedoskonałości | refaktoryzacja kodu |
| funkcje kosmetyczne, ułatawiające tworzenie gry | praca nad zdobywaniem wyników |

