#include "gamestate.h"


void GameState::newQuestion()  {
    /*
        Takes as a parameter number of the round counting from 1-15
        and returns a question parsed to vector of size equal to 5.

        Jestesmy bilingualni wiec piszemy komentarze w dwoch jezykach.

        Poprawna odpowiedz w plikach .csv jest ZAWSZE na drugiej pozycji.
    */


    srand(time(NULL));

    int qNum = rand() % 10 + 1;

    //Windows
    //string pathname = "..\\project_07_35190\\questions\\question" + to_string(round) + ".csv";

    //Linux
    string pathname = "../project_07_35190/questions/question" + to_string(this->activeRound) + ".csv";


    ifstream FILE;
    string line, word;
    vector<string> row;

    FILE.open(pathname);
    if(!FILE.good()) {
        cout << "Blad podczas otwierania pliku. Konczenie pracy programu...";
        exit(1);
    }

    // gettin the right question
    for(int i = 0; i < qNum; i++) {
        getline(FILE, line);
    }

    // splittin it to words
    stringstream curr(line);
    while(getline(curr, word, ',')) {
        row.push_back(word);
    }


    this->correctAnswer = row[1];
    this->question = row;

    random_shuffle(question.begin()+1, question.end());
}

void GameState::clearScreen() {

    if(this->isWindows)
        system("cls");
    else
        system("clear");
}

void GameState::showCreators() {

    this->presentLogo();
    cout << "Bartosz Burzec, nr in. 35190 - kierownictwo projektu, pytania, wizualizacja, logika, nadzor" << endl;
    cout << "Kacper Bialas, nr in. 35187 - pytania, logika, testowanie, wizualizacja" << endl;
    this->sleep(4);

}

void GameState::sleep(int seconds) {

    std::chrono::milliseconds timespan(seconds * 1000);
    std::this_thread::sleep_for(timespan);
}

void GameState::sleep_miliseconds(int miliseconds) {

    std::chrono::milliseconds timespan(miliseconds);
    std::this_thread::sleep_for(timespan);
}

void GameState::borderText(string text) {

    cout << "+";
    for(int i = 0; i < (int)text.length()+2; i++)
        cout << "-";
    cout << "+" << endl;
    cout << "| " + text + " |" << endl;
    cout << "+";
    for(int i = 0; i < (int)text.length()+2; i++)
        cout << "-";
    cout << "+" << endl;
}


void GameState::presentQuestion() {

    cout << "Posiadasz kola ratunkowe:" << endl;
    cout << "[";
    if(useFiftyFifty)
        cout << "50/50 ";
    if(useTelephone)
        cout << "telefon ";
    if(usePoll)
        cout << "pytanie";
    if(!useFiftyFifty && !usePoll && !useTelephone)
        cout << "brak";
    cout << "]" << endl;

    string qu = "Pytanie " + to_string(this->activeRound);
    this->borderText(qu);

    cout << endl << this->question[0] << endl;
    cout << "[Twoj wynik: " << this->playerScore << "$]" << endl;

    cout << "A: " << this->question[1] << endl;
    cout << "B: " << this->question[2] << endl;
    cout << "C: " << this->question[3] << endl;
    cout << "D: " << this->question[4] << endl;
}

void GameState::fiftyFifty() {

    this->clearScreen();

    if(!useFiftyFifty) {
        cout << "Nie mozesz uzyc tego kola ratunkowego!" << endl;
        return;
    }

    cout << "Uzyto kola ratunkowego: 50/50!" << endl << endl;

    this->useFiftyFifty = false;
    srand(time(NULL));
    for(int i = 0; i < 2; i++) {
        int indexToChange = rand() % 3 + 2;

        if( question[indexToChange].compare("") == 0 ||
            question[indexToChange].compare(this->correctAnswer) == 0) {
            i--;
            continue;
        }

        this->question[indexToChange] = "";
    }
}

void GameState::makeChoice() {

    string userChoice;
    cout << "Twoja odpowiedz: ";
    getline(cin, userChoice);

    if(userChoice.length() == 1) {

        char charAnswer = userChoice[0];
        char numAnswer = (charAnswer-1)%4 + 1;
        if(charAnswer < 'A' || charAnswer > 'D') {
            cout << "Nie ma takiej odpowiedzi. Podaj ponownie." << endl;
            return;
        }

        int compToCorrect = this->question[numAnswer].compare(this->correctAnswer);

        if(!compToCorrect) {
            // jesli sa rowne 0

            this->playerScore = this->moneyLevel[this->activeRound - 1];

            if(this->activeRound == 15){
                            this->win();
                        }
            this->clearScreen();
            cout << "To byla poprawna odpowiedz!" << endl;
            this->sleep(1);

            cout << "Twoja nagroda: " << endl;
            this->sleep(1);
            this->borderText(to_string(this->playerScore) + '$');
            this->sleep(1);
            if(this->activeRound % 5 == 0) {
                this->moneyCheckpoint++;
                cout << "Masz gwarantowane " << this->playerScore << "$!" << endl;
            }
//            cout << "Ladowanie kolejnego etapu..." << endl;
//            this->sleep(2);
//            this->activeRound = this->activeRound + 1;
//            this->newQuestion();
//            this->clearScreen();
//            return;

            this->resign();
            return;
        }

        this->clearScreen();
        int finalScore = this->moneyCheckpoint * 5;
        finalScore = finalScore > 0 ? finalScore - 1 : finalScore;
        this->playerScore = finalScore;

        cout << "Niepoprawna odpowiedz - przegrywasz!" << endl;
        this->gameLost = true;
        this->ending();
    }
    else {

        if(userChoice.compare("50/50") == 0)
            this->fiftyFifty();
        else if(userChoice.compare("telefon")==0)
            this->telephone();
        else if(userChoice.compare("pytanie")==0)
            this->poll();
        else {
            this->clearScreen();
            cout << "Podana zla opcje. " << endl;

        }

    }
}

void GameState::presentLogo() {

    string pathname = "../project_07_35190/other/logo.txt";

    ifstream FILE;
    string line;
    vector<string> logo;

    FILE.open(pathname);
    if(!FILE.good()) {
        cout << "Blad podczas otwierania pliku. Konczenie pracy programu...";
        exit(1);
    }

    // gettin the right question
    while(!FILE.eof()) {
        getline(FILE,line);
        logo.push_back(line);
    }

    FILE.close();

    for(int i = 0; i < (int)logo.size(); i++) {
        cout << logo[i] << endl;
        this->sleep_miliseconds(100);
    }


}

void GameState::telephone() {

    this->clearScreen();

    if(!useTelephone) {
        cout << "Nie mozesz uzyc tego kola ratunkowego!" << endl;
        return;
    }

    cout << "Uzyto kola ratunkowego: telefon do przyjaciela" << endl << endl;

    this->useTelephone = false;

    if(this->activeRound <= 5) {
        cout << "Przyjaciel radzi: " << endl;
        cout << "{Wydaje mi sie, ze poprawna odpowiedzia bedzie \"";
        cout << this->correctAnswer;
        cout << "\".}" << endl;
        return;
    }

    srand(time(NULL));
    int quess = rand()%4+1;
    while(this->question[quess].compare("")==0)
        quess = rand()%4+1;
    cout << this->question[quess] << endl;

}

void GameState::poll(){

    this->clearScreen();

    if(!usePoll) {
        cout  << "Nie mozesz uzyc tego kola ratunkowego!" << endl << endl;
        return;
    }

    cout << "Uzyto kola ratunkowego: pytanie do publicznosci" << endl;

    this->usePoll = false;

    srand(time(NULL));
    int abc = rand()%50+51;
    int reszta = 100 - abc;
    int reszta2 = reszta/3;
    int answer = 1;

    if(!useFiftyFifty)
        reszta2=reszta;

    cout << "Wyniki glosowania dla publicznosci: " << endl;
    while(answer <= 4){
    if(this->question[answer].compare(this->correctAnswer)==0 && !useFiftyFifty){
        cout << this->question[answer] << ": " << abc << "%" << endl;
        answer++;
    }
    else if(this->question[answer].compare(this->correctAnswer)==0 && reszta%3 == 1 ){
        cout << this->question[answer] << ": " << abc+1 << "%" << endl;
        answer++;
    }
    else if(this->question[answer].compare(this->correctAnswer)==0 && reszta%3 == 2){
        cout << this->question[answer] << ": " << abc+2 << "%" << endl;
        answer++;
    }
    else if(this->question[answer].compare(this->correctAnswer)==0){
        cout << this->question[answer] << ": " << abc << "%" << endl;
        answer++;
    }
    else if(this->question[answer].compare("")==0 || this->question[answer].compare(this->correctAnswer)==0){
        answer++;
    }
    else{
        cout << this->question[answer] << ": " << reszta2 << "%" << endl;
        answer++;
    }
}

}

void GameState::ending() {

    this->clearScreen();
    cout << "Koniec gry!" << endl;
    cout << "Przegrales na pytaniu numer: ";
    cout << this->activeRound << endl;
    cout << "Poprawna odpowiedz to: ";
    cout << this->correctAnswer << endl;
    cout <<  "Twoj wynik to: " << endl;
    if(this->moneyCheckpoint == 0)
        this->borderText("0$");
    else if(this->moneyCheckpoint == 1)
        this->borderText("1000$");
    else if(this->moneyCheckpoint == 2)
        this->borderText("32000$");
    else if(this->moneyCheckpoint == 3)
        this->borderText("1000000$");
    exit(0);

}

void GameState::resign() {

    cout << "Chcesz grac dalej czy rezygnujesz z gry?(gram/rezygnuje)" << endl;

    string userChoice;
    cout << "Twoja odpowiedz: ";
    getline(cin, userChoice);

    while(userChoice.compare("gram") != 0 && userChoice.compare("rezygnuje") != 0) {
        cout << "Twoja odpowiedz: ";
        getline(cin, userChoice);
    }
    if(userChoice.compare("gram") == 0) {
        cout << "Ladowanie kolejnego etapu..." << endl;
        this->sleep(2);
        this->activeRound = this->activeRound + 1;
        this->newQuestion();
        this->clearScreen();
        return;
    }
    else if(userChoice.compare("rezygnuje") == 0){
        this->clearScreen();
        cout << "Koniec gry!" << endl;
        cout << "Zrezygnowales na pytaniu numer: ";
        cout << this->activeRound << endl;
        cout <<  "Twoj wynik to: " << endl;
        this->borderText(to_string(this->playerScore) + '$');
    }

        exit(0);


}

void GameState::win(){

    this->clearScreen();
    cout << "Koniec gry!" << endl;
    cout << "Odpowiedziales na wszystkie pytania!" << endl;
    cout << "Twoja nagroda to: " << endl;
    this->borderText("1000000$");
    cout << "Gratulacje!" << endl;
    exit(0);

}

